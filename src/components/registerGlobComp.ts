import type { App } from 'vue';
import { Button } from './Button';
import { Input, Layout, Select, Switch } from 'ant-design-vue';
import 'xe-utils';
import VXETable from 'vxe-table';
import VXETablePluginAntd from 'vxe-table-plugin-antd';
import 'vxe-table/lib/style.css';
import 'highlight.js/styles/github-dark.css';
import 'highlight.js/lib/common';
import hljsVuePlugin from '@highlightjs/vue-plugin';

export function registerGlobComp(app: App) {
  // VXETable 全局配置
  const VXETableSettings = {
    // z-index 起始值
    zIndex: 1000,
    table: {},
  };
  VXETable.use(VXETablePluginAntd);
  app.use(Input).use(Button).use(Select).use(Switch).use(Layout).use(VXETable, VXETableSettings).use(hljsVuePlugin);
}
