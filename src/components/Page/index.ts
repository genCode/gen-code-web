import { withInstall } from '/@/utils';

import pageFooter from './src/PageFooter.vue';
import pageWrapper from './src/PageWrapper.vue';

export const PageFooter = withInstall(pageFooter);
console.log("pageWrapper",pageWrapper)
export const PageWrapper = withInstall(pageWrapper);

export const PageWrapperFixedHeightKey = 'PageWrapperFixedHeight';
