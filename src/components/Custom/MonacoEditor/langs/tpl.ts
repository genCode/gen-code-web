/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\components\Custom\MonacoEditor\langs\tpl.ts
 */
import utils from '/@/crud/utils/index';
import { cloneDeep } from 'lodash-es';
interface keywordsInter {
  label: string;
  detail: string;
  children?: keywordsInter[];
  [key: string]: any;
}
const keywords = (range) =>
  [
    { label: 'getJsonSize', detail: '' },
    { label: 'CaseCamel', detail: '首字母大写驼峰' },
    { label: 'CaseCamelLower', detail: '首字母小写驼峰' },
    { label: 'ContainsI', detail: '是否包含子字符串' },
    { label: 'HasSuffix', detail: '是否存在后缀' },
    { label: 'ImportList', detail: '导入包集合' },
    { label: 'InArray', detail: '是否在数组中' },
    { label: 'PublicFields', detail: '公共字段' },
    { label: 'SnakeScreamingCase', detail: '转常量名' },
    { label: 'Sum', detail: '求和' },
    {
      label: 'Table',
      detail: '',
      range,
      children: [
        {
          label: 'TableId',
          detail: '编号',
        },
        {
          label: 'TableName',
          detail: '表名称',
        },
        {
          label: 'TableComment',
          detail: '表描述',
        },
        {
          label: 'TplCategory',
          detail: '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
        },
        {
          label: 'SubTableName',
          detail: '关联子表的表名',
        },
        {
          label: 'SubTableFkName',
          detail: '子表关联的外键名',
        },
        {
          label: 'TreeCode',
          detail: '树编码字段',
        },
        {
          label: 'TreeName',
          detail: '树编码字段',
        },
        {
          label: 'TreeParentCode',
          detail: '树编码字段',
        },
        {
          label: 'ClassName',
          detail: '实体类名称',
        },
        {
          label: 'SystemName',
          detail: '系统名称',
        },
        {
          label: 'ModuleName',
          detail: '生成模块名',
        },
        {
          label: 'PackageName',
          detail: '生成包路径',
        },
        {
          label: 'BusinessName',
          detail: '生成业务名',
        },
        {
          label: 'FunctionName',
          detail: '生成功能名',
        },
        {
          label: 'FunctionAuthor',
          detail: '生成功能作者',
        },
        {
          label: 'GenType',
          detail: '生成代码方式（0zip压缩包 1自定义路径）',
        },
        {
          label: 'GenPath',
          detail: '生成路径（不填默认项目路径）',
        },
        {
          label: 'Params',
          detail: '额外属性',
        },
        {
          label: 'CreateBy',
          detail: '创建者',
        },
        {
          label: 'CreateTime',
          detail: '创建时间',
        },
        {
          label: 'UpdateBy',
          detail: '更新者',
        },
        {
          label: 'UpdateTime',
          detail: '更新时间',
        },
        {
          label: 'Remark',
          detail: '备注',
        },
        {
          label: 'PkColumn',
          detail: '主键信息',
          children: [
            {
              label: 'ColumnId',
              detail: '编号',
            },
            {
              label: 'TableId',
              detail: '归属表编号',
            },
            {
              label: 'ColumnName',
              detail: '列名称',
            },
            {
              label: 'ColumnComment',
              detail: '列描述',
            },
            {
              label: 'ColumnType',
              detail: '列类型',
            },
            {
              label: 'ColumnLength',
              detail: '列长度',
            },
            {
              label: 'JavaType',
              detail: 'JAVA类型',
            },
            {
              label: 'JavaField',
              detail: 'JAVA字段名',
            },
            {
              label: 'GoType',
              detail: 'GO类型',
            },
            {
              label: 'GoField',
              detail: 'GO字段名',
            },
            {
              label: 'TsType',
              detail: 'TypeScript类型',
            },
            {
              label: 'TsField',
              detail: 'TypeScript字段名',
            },
            {
              label: 'IsPk',
              detail: '是否主键（1是）',
            },
            {
              label: 'IsIncrement',
              detail: '是否自增（1是）',
            },
            {
              label: 'IsRequired',
              detail: '是否必填（1是）',
            },
            {
              label: 'IsInsert',
              detail: '是否为插入字段（1是）',
            },
            {
              label: 'IsEdit',
              detail: '是否编辑字段（1是）',
            },
            {
              label: 'IsList',
              detail: '是否列表字段（1是）',
            },
            {
              label: 'IsQuery',
              detail: '是否查询字段（1是）',
            },
            {
              label: 'QueryType',
              detail: '查询方式（等于、不等于、大于、小于、范围）',
            },
            {
              label: 'HtmlType',
              detail: '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
            },
            {
              label: 'DictType',
              detail: '字典类型',
            },
            {
              label: 'Sort',
              detail: '排序',
            },
            {
              label: 'CreateBy',
              detail: '创建者',
            },
            {
              label: 'CreateTime',
              detail: '创建时间',
            },
            {
              label: 'UpdateBy',
              detail: '更新者',
            },
            {
              label: 'UpdateTime',
              detail: '更新时间',
            },
          ],
        },
        {
          label: 'SubTable',
          detail: '子表信息',
          children: [
            {
              label: 'TableId',
              detail: '编号',
            },
            {
              label: 'TableName',
              detail: '表名称',
            },
            {
              label: 'TableComment',
              detail: '表描述',
            },
            {
              label: 'TplCategory',
              detail: '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
            },
            {
              label: 'SubTableName',
              detail: '关联子表的表名',
            },
            {
              label: 'SubTableFkName',
              detail: '子表关联的外键名',
            },
            {
              label: 'TreeCode',
              detail: '树编码字段',
            },
            {
              label: 'TreeName',
              detail: '树编码字段',
            },
            {
              label: 'TreeParentCode',
              detail: '树编码字段',
            },
            {
              label: 'ClassName',
              detail: '实体类名称',
            },
            {
              label: 'SystemName',
              detail: '系统名称',
            },
            {
              label: 'ModuleName',
              detail: '生成模块名',
            },
            {
              label: 'PackageName',
              detail: '生成包路径',
            },
            {
              label: 'BusinessName',
              detail: '生成业务名',
            },
            {
              label: 'FunctionName',
              detail: '生成功能名',
            },
            {
              label: 'FunctionAuthor',
              detail: '生成功能作者',
            },
            {
              label: 'GenType',
              detail: '生成代码方式（0zip压缩包 1自定义路径）',
            },
            {
              label: 'GenPath',
              detail: '生成路径（不填默认项目路径）',
            },
            {
              label: 'Params',
              detail: '额外属性',
            },
            {
              label: 'CreateBy',
              detail: '创建者',
            },
            {
              label: 'CreateTime',
              detail: '创建时间',
            },
            {
              label: 'UpdateBy',
              detail: '更新者',
            },
            {
              label: 'UpdateTime',
              detail: '更新时间',
            },
            {
              label: 'Remark',
              detail: '备注',
            },
            {
              label: 'PkColumn',
              detail: '主键信息',
            },
            {
              label: 'SubTable',
              detail: '子表信息',
            },
            {
              label: 'Columns',
              detail: '字段集合',
            },
          ],
        },
        {
          label: 'Columns',
          detail: '字段集合',
          children: [
            {
              label: 'ColumnId',
              detail: '编号',
            },
            {
              label: 'TableId',
              detail: '归属表编号',
            },
            {
              label: 'ColumnName',
              detail: '列名称',
            },
            {
              label: 'ColumnComment',
              detail: '列描述',
            },
            {
              label: 'ColumnType',
              detail: '列类型',
            },
            {
              label: 'ColumnLength',
              detail: '列长度',
            },
            {
              label: 'JavaType',
              detail: 'JAVA类型',
            },
            {
              label: 'JavaField',
              detail: 'JAVA字段名',
            },
            {
              label: 'GoType',
              detail: 'GO类型',
            },
            {
              label: 'GoField',
              detail: 'GO字段名',
            },
            {
              label: 'TsType',
              detail: 'TypeScript类型',
            },
            {
              label: 'TsField',
              detail: 'TypeScript字段名',
            },
            {
              label: 'IsPk',
              detail: '是否主键（1是）',
            },
            {
              label: 'IsIncrement',
              detail: '是否自增（1是）',
            },
            {
              label: 'IsRequired',
              detail: '是否必填（1是）',
            },
            {
              label: 'IsInsert',
              detail: '是否为插入字段（1是）',
            },
            {
              label: 'IsEdit',
              detail: '是否编辑字段（1是）',
            },
            {
              label: 'IsList',
              detail: '是否列表字段（1是）',
            },
            {
              label: 'IsQuery',
              detail: '是否查询字段（1是）',
            },
            {
              label: 'QueryType',
              detail: '查询方式（等于、不等于、大于、小于、范围）',
            },
            {
              label: 'HtmlType',
              detail: '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
            },
            {
              label: 'DictType',
              detail: '字典类型',
            },
            {
              label: 'Sort',
              detail: '排序',
            },
            {
              label: 'CreateBy',
              detail: '创建者',
            },
            {
              label: 'CreateTime',
              detail: '创建时间',
            },
            {
              label: 'UpdateBy',
              detail: '更新者',
            },
            {
              label: 'UpdateTime',
              detail: '更新时间',
            },
          ],
        },
      ],
    },
    { label: 'UcFirst', detail: '' },
    { label: 'VueTag', detail: '' },
  ] as Array<keywordsInter>;
const config = {
  brackets: [
    ['{{', '}}'],
    ['{', '}'],
  ],
  autoClosingPairs: [
    { open: '{{', close: '}}' },
    { open: '{', close: '}' },
    { open: '[', close: ']' },
    { open: '(', close: ')' },
    { open: '"', close: '"', notIn: ['string'] },
    { open: "'", close: "'", notIn: ['string', 'comment'] },
    { open: '`', close: '`', notIn: ['string', 'comment'] },
    { open: '/**', close: ' */', notIn: ['string'] },
  ],
  autoCloseBefore: '-.}) \n\t',
  surroundingPairs: [
    ['(', ')'],
    ['"', '"'],
    ['`', '`'],
  ],
  comments: {
    // symbols used for start and end a block comment. Remove this entry if your language does not support block comments
    blockComment: ['/*', '*/'],
  },
};
export const getLanguage = (monaco) => {
  let kind = monaco.languages.CompletionItemKind.Function,
    insertTextRules = null || monaco.languages.CompletionItemInsertTextRule.InsertAsSnippet;
  const getLastData = (content, index) => {
    content = content.substring(0, index - 1);
    let splitRes = content.trim().replace(/\s+/g, ' ').replace(/,/g, ', ').replace(/=/g, '= ').replace(/\(/g, '( ')?.split(' ');
    let res = splitRes[splitRes.length - 1];
    return res.substring(0, res.length - 1);
  };

  // 注册语言
  monaco.languages.register({ id: 'tpl' });
  monaco.languages.setLanguageConfiguration('tpl', config);
  return monaco.languages.registerCompletionItemProvider('tpl', {
    provideCompletionItems: (model, position) => {
      var range = {
        startLineNumber: position.lineNumber,
        endLineNumber: position.lineNumber,
        startColumn: model.getWordUntilPosition(position).startColumn,
        endColumn: model.getWordUntilPosition(position).endColumn,
      };
      var textUntilPosition = model.getValueInRange({
        startLineNumber: 1,
        startColumn: 1,
        endLineNumber: position.lineNumber,
        endColumn: position.column,
      });
      const line = position.lineNumber;
      const content = model.getLineContent(line);
      let data = getLastData(content, position.column); //这个不知道是啥 先用着
      let property = data.split(['.']);
      var match = textUntilPosition.match(/\s*$/);
      var suggestions = match ? keywords(range) : [];

      // 根据条件获取建议
      const getSug = (list, fun: (item: any) => Array<any>, path = []) => {
        list.forEach((item: any) => {
          if (item?.children) {
            path = path.concat(
              item?.children.map((_v) => {
                // 有children会导致提示报错
                delete _v?.children;
                return { ..._v, kind, insertText: _v?.insertText || _v.label };
              }),
            );
          }
          if (fun(item)) return path;
          if (item?.children && getSug(item?.children, fun, path)?.length > 0) return getSug(item?.children, fun, path);
          path.pop();
        });
        return path;
      };
      /*
       * 拿到字符串分组的最后一个
       */
      const field = property[property.length - 1];
      //匹配第一个点
      if (textUntilPosition.charAt(textUntilPosition.length - 1) == '.' && property.length < 2) {
        //当是点时匹配自定义的方法 ...
        return {
          suggestions:
            cloneDeep(suggestions).map((item: any) => {
              delete item?.children;
              return { ...item, kind, insertText: item?.insertText || item.label };
            }) ?? [],
        };
        // 匹配除第一个点外的点
      } else {
        const arr: keywordsInter = utils.treeFind({
          tree: suggestions,
          func: (item) => {
            return item.label == field;
          },
        });
        var result = getSug(cloneDeep([arr]), (res) => res?.children && field == arr?.label);
        return { suggestions: result ?? [] };
      }
    },
    triggerCharacters: ['.'], // 写触发提示的字符，可以有多个
  });
};
