/*
 * @version:
 * @Company:  
 * @Description:
 * @FilePath: \src\components\Custom\MonacoEditor\language.tsx
 * @since: 2022-03-30
 */

import * as lang from './lang';
import { getLanguage as getTpl } from './langs/tpl';

export const getLanguageSelect = () => [
  // { label: 'abap', value: 'abap' },
  // { label: 'apex', value: 'apex' },
  // { label: 'azcli', value: 'azcli' },
  // { label: 'bat', value: 'bat' },
  // { label: 'bicep', value: 'bicep' },
  // { label: 'cameligo', value: 'cameligo' },
  // { label: 'clojure', value: 'clojure' },
  // { label: 'coffee', value: 'coffee' },
  // { label: 'cpp', value: 'cpp' },
  // { label: 'csharp', value: 'csharp' },
  // { label: 'csp', value: 'csp' },
  { label: 'css', value: 'css' },
  // { label: 'dart', value: 'dart' },
  // { label: 'dockerfile', value: 'dockerfile' },
  // { label: 'ecl', value: 'ecl' },
  // { label: 'elixir', value: 'elixir' },
  // { label: 'flow9', value: 'flow9' },
  // { label: 'freemarker2', value: 'freemarker2' },
  // { label: 'fsharp', value: 'fsharp' },
  { label: 'go', value: 'go' },
  // { label: 'graphql', value: 'graphql' },
  // { label: 'handlebars', value: 'handlebars' },
  // { label: 'hcl', value: 'hcl' },
  { label: 'html', value: 'html' },
  // { label: 'ini', value: 'ini' },
  { label: 'json', value: 'json' },
  { label: 'java', value: 'java' },
  { label: 'javascript', value: 'javascript' },
  // { label: 'julia', value: 'julia' },
  // { label: 'kotlin', value: 'kotlin' },
  { label: 'less', value: 'less' },
  // { label: 'lexon', value: 'lexon' },
  // { label: 'liquid', value: 'liquid' },
  // { label: 'lua', value: 'lua' },
  // { label: 'm3', value: 'm3' },
  { label: 'markdown', value: 'markdown' },
  // { label: 'mips', value: 'mips' },
  // { label: 'msdax', value: 'msdax' },
  { label: 'mysql', value: 'mysql' },
  // { label: 'objective-c', value: 'objective-c' },
  // { label: 'pascal', value: 'pascal' },
  // { label: 'pascaligo', value: 'pascaligo' },
  // { label: 'perl', value: 'perl' },
  // { label: 'pgsql', value: 'pgsql' },
  { label: 'php', value: 'php' },
  // { label: 'pla', value: 'pla' },
  // { label: 'postiats', value: 'postiats' },
  // { label: 'powerquery', value: 'powerquery' },
  { label: 'powershell', value: 'powershell' },
  // { label: 'protobuf', value: 'protobuf' },
  // { label: 'pug', value: 'pug' },
  { label: 'python', value: 'python' },
  // { label: 'qsharp', value: 'qsharp' },
  // { label: 'r', value: 'r' },
  // { label: 'razor', value: 'razor' },
  { label: 'redis', value: 'redis' },
  // { label: 'redshift', value: 'redshift' },
  // { label: 'restructuredtext', value: 'restructuredtext' },
  // { label: 'ruby', value: 'ruby' },
  // { label: 'rust', value: 'rust' },
  // { label: 'sb', value: 'sb' },
  // { label: 'scala', value: 'scala' },
  // { label: 'scheme', value: 'scheme' },
  { label: 'scss', value: 'scss' },
  { label: 'shell', value: 'shell' },
  // { label: 'solidity', value: 'solidity' },
  // { label: 'sophia', value: 'sophia' },
  // { label: 'sparql', value: 'sparql' },
  { label: 'sql', value: 'sql' },
  // { label: 'st', value: 'st' },
  // { label: 'swift', value: 'swift' },
  // { label: 'systemverilog', value: 'systemverilog' },
  // { label: 'tcl', value: 'tcl' },
  // { label: 'twig', value: 'twig' },
  { label: 'typescript', value: 'typescript' },
  // { label: 'vb', value: 'vb' },
  { label: 'xml', value: 'xml' },
  { label: 'yaml', value: 'yaml' },
  { label: 'tpl', value: 'tpl' },
];
export const initLang = (monaco) => {
  let arr: any = [],
    registers = null;
  getLanguageSelect().forEach((el: any) => {
    let item = el.label;
    if (item == 'tpl') {
      registers = getTpl(monaco);
    } else {
      registers = monaco.languages.registerCompletionItemProvider(item, {
        provideCompletionItems: function (model, position) {
          var textUntilPosition = model.getValueInRange({
            startLineNumber: position?.lineNumber ?? 1,
            startColumn: 1,
            endLineNumber: position?.lineNumber ?? 1,
            endColumn: position?.column ?? 1,
          });
          var match: any = textUntilPosition.match(/(\S+)$/);
          if (!match) return [];
          match = match[0].toUpperCase();
          var suggestions: any = [];
          lang?.[`${item}Language`]?.keywords?.forEach((item) => {
            suggestions.push({
              label: item,
              kind: monaco.languages.CompletionItemKind.Keyword,
              insertText: item,
            });
          });
          // 由于官方php的属性格式与其他不同 需特殊处理
          lang?.[`${item}Language`]?.[`${item}Keywords`]?.forEach((item) => {
            suggestions.push({
              label: item,
              kind: monaco.languages.CompletionItemKind.Keyword,
              insertText: item,
            });
          });
          lang?.[`${item}Language`]?.operators?.forEach((item) => {
            if (item.indexOf(match) !== -1) {
              suggestions.push({
                label: item,
                kind: monaco.languages.CompletionItemKind.Operator,
                insertText: item,
              });
            }
          });
          lang?.[`${item}Language`]?.builtinFunctions?.forEach((item) => {
            if (item.indexOf(match) !== -1) {
              suggestions.push({
                label: item,
                kind: monaco.languages.CompletionItemKind.Function,
                insertText: item,
              });
            }
          });
          return {
            suggestions,
          };
        },
      });
    }
    arr.push({ ...el, registers });
  });
  return arr;
};
