/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\components\Custom\MonacoEditor\lang.ts
 */
// import { language as abapLanguage } from 'monaco-editor/esm/vs/basic-languages/abap/abap.js';
// import { language as apexLanguage } from 'monaco-editor/esm/vs/basic-languages/apex/apex.js';
// import { language as azcliLanguage } from 'monaco-editor/esm/vs/basic-languages/azcli/azcli.js';
// import { language as batLanguage } from 'monaco-editor/esm/vs/basic-languages/bat/bat.js';
// import { language as bicepLanguage } from 'monaco-editor/esm/vs/basic-languages/bicep/bicep.js';
// import { language as cameligoLanguage } from 'monaco-editor/esm/vs/basic-languages/cameligo/cameligo.js';
// import { language as clojureLanguage } from 'monaco-editor/esm/vs/basic-languages/clojure/clojure.js';
// import { language as coffeeLanguage } from 'monaco-editor/esm/vs/basic-languages/coffee/coffee.js';
// import { language as cppLanguage } from 'monaco-editor/esm/vs/basic-languages/cpp/cpp.js';
// import { language as csharpLanguage } from 'monaco-editor/esm/vs/basic-languages/csharp/csharp.js';
// import { language as cspLanguage } from 'monaco-editor/esm/vs/basic-languages/csp/csp.js';
import { language as cssLanguage } from 'monaco-editor/esm/vs/basic-languages/css/css.js';
// import { language as dartLanguage } from 'monaco-editor/esm/vs/basic-languages/dart/dart.js';
// import { language as dockerfileLanguage } from 'monaco-editor/esm/vs/basic-languages/dockerfile/dockerfile.js';
// import { language as eclLanguage } from 'monaco-editor/esm/vs/basic-languages/ecl/ecl.js';
// import { language as elixirLanguage } from 'monaco-editor/esm/vs/basic-languages/elixir/elixir.js';
// import { language as flow9Language } from 'monaco-editor/esm/vs/basic-languages/flow9/flow9.js';
// import { language as freemarker2Language } from 'monaco-editor/esm/vs/basic-languages/freemarker2/freemarker2.js';
// import { language as fsharpLanguage } from 'monaco-editor/esm/vs/basic-languages/fsharp/fsharp.js';
import { language as goLanguage } from 'monaco-editor/esm/vs/basic-languages/go/go.js';
// import { language as graphqlLanguage } from 'monaco-editor/esm/vs/basic-languages/graphql/graphql.js';
// import { language as handlebarsLanguage } from 'monaco-editor/esm/vs/basic-languages/handlebars/handlebars.js';
// import { language as hclLanguage } from 'monaco-editor/esm/vs/basic-languages/hcl/hcl.js';
import { language as htmlLanguage } from 'monaco-editor/esm/vs/basic-languages/html/html.js';
// import { language as iniLanguage } from 'monaco-editor/esm/vs/basic-languages/ini/ini.js';
import { language as javaLanguage } from 'monaco-editor/esm/vs/basic-languages/java/java.js';
import { language as javascriptLanguage } from 'monaco-editor/esm/vs/basic-languages/javascript/javascript.js';
// import { language as juliaLanguage } from 'monaco-editor/esm/vs/basic-languages/julia/julia.js';
// import { language as kotlinLanguage } from 'monaco-editor/esm/vs/basic-languages/kotlin/kotlin.js';
import { language as lessLanguage } from 'monaco-editor/esm/vs/basic-languages/less/less.js';
// import { language as lexonLanguage } from 'monaco-editor/esm/vs/basic-languages/lexon/lexon.js';
// import { language as liquidLanguage } from 'monaco-editor/esm/vs/basic-languages/liquid/liquid.js';
// import { language as luaLanguage } from 'monaco-editor/esm/vs/basic-languages/lua/lua.js';
// import { language as m3Language } from 'monaco-editor/esm/vs/basic-languages/m3/m3.js';
import { language as markdownLanguage } from 'monaco-editor/esm/vs/basic-languages/markdown/markdown.js';
// import { language as mipsLanguage } from 'monaco-editor/esm/vs/basic-languages/mips/mips.js';
// import { language as msdaxLanguage } from 'monaco-editor/esm/vs/basic-languages/msdax/msdax.js';
import { language as mysqlLanguage } from 'monaco-editor/esm/vs/basic-languages/mysql/mysql.js';
// import { language as objectivecLanguage } from 'monaco-editor/esm/vs/basic-languages/objective-c/objective-c.js';
// import { language as pascalLanguage } from 'monaco-editor/esm/vs/basic-languages/pascal/pascal.js';
// import { language as pascaligoLanguage } from 'monaco-editor/esm/vs/basic-languages/pascaligo/pascaligo.js';
// import { language as perlLanguage } from 'monaco-editor/esm/vs/basic-languages/perl/perl.js';
// import { language as pgsqlLanguage } from 'monaco-editor/esm/vs/basic-languages/pgsql/pgsql.js';
import { language as phpLanguage } from 'monaco-editor/esm/vs/basic-languages/php/php.js';
// import { language as plaLanguage } from 'monaco-editor/esm/vs/basic-languages/pla/pla.js';
// import { language as postiatsLanguage } from 'monaco-editor/esm/vs/basic-languages/postiats/postiats.js';
// import { language as powerqueryLanguage } from 'monaco-editor/esm/vs/basic-languages/powerquery/powerquery.js';
import { language as powershellLanguage } from 'monaco-editor/esm/vs/basic-languages/powershell/powershell.js';
// import { language as protobufLanguage } from 'monaco-editor/esm/vs/basic-languages/protobuf/protobuf.js';
// import { language as pugLanguage } from 'monaco-editor/esm/vs/basic-languages/pug/pug.js';
import { language as pythonLanguage } from 'monaco-editor/esm/vs/basic-languages/python/python.js';
// import { language as qsharpLanguage } from 'monaco-editor/esm/vs/basic-languages/qsharp/qsharp.js';
// import { language as rLanguage } from 'monaco-editor/esm/vs/basic-languages/r/r.js';
// import { language as razorLanguage } from 'monaco-editor/esm/vs/basic-languages/razor/razor.js';
import { language as redisLanguage } from 'monaco-editor/esm/vs/basic-languages/redis/redis.js';
// import { language as redshiftLanguage } from 'monaco-editor/esm/vs/basic-languages/redshift/redshift.js';
// import { language as restructuredtextLanguage } from 'monaco-editor/esm/vs/basic-languages/restructuredtext/restructuredtext.js';
// import { language as rubyLanguage } from 'monaco-editor/esm/vs/basic-languages/ruby/ruby.js';
// import { language as rustLanguage } from 'monaco-editor/esm/vs/basic-languages/rust/rust.js';
// import { language as sbLanguage } from 'monaco-editor/esm/vs/basic-languages/sb/sb.js';
// import { language as scalaLanguage } from 'monaco-editor/esm/vs/basic-languages/scala/scala.js';
// import { language as schemeLanguage } from 'monaco-editor/esm/vs/basic-languages/scheme/scheme.js';
// import { language as scssLanguage } from 'monaco-editor/esm/vs/basic-languages/scss/scss.js';
import { language as shellLanguage } from 'monaco-editor/esm/vs/basic-languages/shell/shell.js';
// import { language as solidityLanguage } from 'monaco-editor/esm/vs/basic-languages/solidity/solidity.js';
// import { language as sophiaLanguage } from 'monaco-editor/esm/vs/basic-languages/sophia/sophia.js';
// import { language as sparqlLanguage } from 'monaco-editor/esm/vs/basic-languages/sparql/sparql.js';
import { language as sqlLanguage } from 'monaco-editor/esm/vs/basic-languages/sql/sql.js';
// import { language as stLanguage } from 'monaco-editor/esm/vs/basic-languages/st/st.js';
// import { language as swiftLanguage } from 'monaco-editor/esm/vs/basic-languages/swift/swift.js';
// import { language as systemverilogLanguage } from 'monaco-editor/esm/vs/basic-languages/systemverilog/systemverilog.js';
import { language as tclLanguage } from 'monaco-editor/esm/vs/basic-languages/tcl/tcl.js';
// import { language as twigLanguage } from 'monaco-editor/esm/vs/basic-languages/twig/twig.js';
import { language as typescriptLanguage } from 'monaco-editor/esm/vs/basic-languages/typescript/typescript.js';
// import { language as vbLanguage } from 'monaco-editor/esm/vs/basic-languages/vb/vb.js';
import { language as xmlLanguage } from 'monaco-editor/esm/vs/basic-languages/xml/xml.js';
import { language as yamlLanguage } from 'monaco-editor/esm/vs/basic-languages/yaml/yaml.js';
// import { language as tplLanguage } from './langs/tpl';
export {
  // abapLanguage,
  // apexLanguage,
  // azcliLanguage,
  // batLanguage,
  // bicepLanguage,
  // cameligoLanguage,
  // clojureLanguage,
  // coffeeLanguage,
  // cppLanguage,
  // csharpLanguage,
  // cspLanguage,
  cssLanguage,
  // dartLanguage,
  // dockerfileLanguage,
  // eclLanguage,
  // elixirLanguage,
  // flow9Language,
  //   freemarker2Language,
  // fsharpLanguage,
  goLanguage,
  // graphqlLanguage,
  // handlebarsLanguage,
  // hclLanguage,
  htmlLanguage,
  // iniLanguage,
  javaLanguage,
  javascriptLanguage,
  // juliaLanguage,
  // kotlinLanguage,
  lessLanguage,
  // lexonLanguage,
  // liquidLanguage,
  // luaLanguage,
  // m3Language,
  markdownLanguage,
  // mipsLanguage,
  // msdaxLanguage,
  mysqlLanguage,
  // objectivecLanguage,
  // pascalLanguage,
  // pascaligoLanguage,
  // perlLanguage,
  // pgsqlLanguage,
  phpLanguage,
  // plaLanguage,
  // postiatsLanguage,
  // powerqueryLanguage,
  powershellLanguage,
  // protobufLanguage,
  // pugLanguage,
  pythonLanguage,
  // qsharpLanguage,
  // rLanguage,
  // razorLanguage,
  redisLanguage,
  // redshiftLanguage,
  // restructuredtextLanguage,
  // rubyLanguage,
  // rustLanguage,
  // sbLanguage,
  // scalaLanguage,
  // schemeLanguage,
  // scssLanguage,
  shellLanguage,
  // solidityLanguage,
  // sophiaLanguage,
  // sparqlLanguage,
  sqlLanguage,
  // stLanguage,
  // swiftLanguage,
  // systemverilogLanguage,
  tclLanguage,
  // twigLanguage,
  typescriptLanguage,
  // vbLanguage,
  xmlLanguage,
  yamlLanguage,
  // tplLanguage
};
