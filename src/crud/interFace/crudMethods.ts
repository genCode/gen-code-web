/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\crud\interFace\crudMethods.ts
 */

interface callbackInter {
  crud?: any;
  res?: any;
}
export interface baseParamsInter {
  params: null | any;
  api: (() => Promise<any>) | null;
  before: ((event: callbackInter) => Promise<any>) | null;
  after: ((event: callbackInter) => Promise<any>) | null;
}
/*
 * 删除
 */
export interface toDelInter extends baseParamsInter {
  useConfirm: boolean;
  refresh: boolean;
}

/*
 * 详情
 */
export interface toDetailInter extends baseParamsInter {
  useConfirm: boolean;
}
