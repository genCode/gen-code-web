/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\crud\interFace\crud.ts
 */
export interface defaultOptsInter {
  title: string;
  defaultParams: { [key: string]: any };
  forms: {
    [key: string]: {
      model: { [key: string]: any };
      defaultForm: { [key: string]: any };
      options: Array<any>;
    };
  };
  times: {
    [key: string]: Number;
  };
  loading: {
    [key: string]: Boolean;
  };
  dialog: {
    title: string;
    visible: Boolean;
  };
  crudMethod: {
    [key in METHODTYPES]: Promise<void> | null;
  };
  successResCode: Array<number>;
  props: any;
  useForm: any;
}
// 方法的类型
enum METHODTYPES {
  addAPI = 'addAPI',
  delAPI = 'delAPI',
  editAPI = 'editAPI',
  saveAPI = 'saveAPI',
  getAPI = 'getAPI',
  detailAPI = 'detailAPI',
}
