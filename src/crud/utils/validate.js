/*
 * @Description:
 * @version:
 * @Company:
 */
/* eslint-disable */
const isNull = (val) => val == null || val == undefined || val == '';
const requiredBlur = { required: true, message: '此项必填', trigger: ['blur'] },
  requiredChange = { required: true, message: '此项必填', trigger: ['change'] },
  requiredInput = { required: true, message: '此项必填', trigger: ['input'] },
  defaultProps = { label: 'name', value: 'code' },
  // 验证手机号码
  verifyPhone = {
    validator: (rule, value, callback) => {
      if (isNull(value)) return callback();
      if (!/^(13[0-9]|14[57]|15[012356789]|17[0678]|18[0-9])[0-9]{8}$/.test(value)) {
        return callback('手机号码不合法，请重新输入');
      }
      callback();
    },
    trigger: 'blur',
  },
  // 验证0
  verifyZero = {
    validator: (rule, value, callback) => {
      if (isNull(value)) return callback();
      if (value == 0 || value > 10) {
        return callback('请输入1-10之间的正整数');
      }
      callback();
    },
    trigger: 'blur',
  },
  // 验证邮箱
  verifyEmail = {
    validator: (rule, value, callback) => {
      if (isNull(value)) return callback();
      if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value)) {
        return callback('邮箱不合法，请重新输入');
      }
      callback();
    },
    trigger: 'blur',
  },
  verifyCreditCode = {
    validator: (rule, value, callback) => {
      if (isNull(value)) return callback();
      if (!/[0-9A-HJ-NPQRTUWXY]{2}\d{6}[0-9A-HJ-NPQRTUWXY]{10}/.test(value)) {
        return callback('不是有效的统一社会信用编码！');
      }
      callback();
    },
    trigger: 'blur',
  },
  verifyAmount = {
    validator: (rule, value, callback) => {
      if (isNull(value)) return callback();
      let reg = /^(([1-9][0-9]*)|(([0]\.\d{1,}|[1-9][0-9]*\.\d{1,})))$/;
      if (!reg.test(value)) {
        callback('请输入的正数');
      }
      reg = /^(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/;
      if (!reg.test(value)) {
        callback('请输入保留2位小数');
      }
      callback();
    },
    trigger: 'blur',
  },
  verifyInteger = { type: 'integer', message: '必须是整数类型' },
  //前后空格
  verifySpace = {
    validator: (rule, value, callback) => {
      if (isNull(value)) {
        return callback('不能为空字符串');
      }
      if (/^\s+|\s+$/g.test(value)) {
        return callback('前后不允许有空格');
      }
      callback();
    },
    type: 'blur',
  }, // 验证整数(不包括0)
  verifyIntNoZero = {
    validator: (rule, value, callback) => {
      // console.log("value", isNull(value));
      if (isNull(value)) return callback();
      if (!/^[1-9]\d*$/g.test(value)) {
        return callback('请输入正整数(不能为0)');
      }
      callback();
    },
    type: 'integer',
  },
  // 验证整数(包括0) 不能以0开头
  verifyIntZero = {
    validator: (rule, value, callback) => {
      if (isNull(value)) return callback();
      if (!/^[1-9]\d*|0$/g.test(value)) {
        return callback('请输入正整数(不能以0开头)');
      }
      callback();
    },
    type: 'integer',
  };
export { requiredBlur, verifyInteger, requiredChange, defaultProps, requiredInput, verifyPhone, verifyEmail, verifyZero, verifyAmount, verifyCreditCode, verifySpace, verifyIntNoZero, verifyIntZero };
/* eslint-disable no-new */
