/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\crud\core\init.ts
 */
import { reactive } from 'vue';
import { crudMethods } from './crudMethods';
import { defaultOptsInter } from '../interFace/crud';
const initCRUD = (crud) => {
  crud = reactive({ ...crud });
  Object.assign(crud, { ...crudMethods(crud) });
  return crud;
};

const CRUD = (options) => {
  const defaultOpts: defaultOptsInter = {
    title: '',
    defaultParams: {},
    /* 表单对象 */
    forms: {
      form: {
        model: {},
        defaultForm: {},
        options: [],
      },
    },
    useForm:{},
    times: {
      refresh: 500,
      operation: 1000,
      timeout: 5000,
    },
    /* Loading */
    loading: {
      dialog: false,
      grid: false,
      form: false,
    },
    /* 模态框 */
    dialog: {
      title: '',
      visible: false,
    },
    /* 请求方法 */
    crudMethod: {
      addAPI: null,
      delAPI: null,
      editAPI: null,
      saveAPI: null,
      getAPI: null,
      detailAPI: null,
    },
    /* 成功的状态码 */
    successResCode: [200],
    /* 自定义属性 */
    props: {},
  };
  options = mergeParams(defaultOpts, options);
  const data = {
    ...options,
    // 主页分页参数
    pages: {
      page: {
        currentPage: 1,
        layout: 'total, sizes, prev, pager, next, jumper',
        pageSizes: [25, 50, 80, 100],
        pageSize: 25,
        total: 0,
      },
    },
    /* 表格对象 */
    grids: {
      grid: {
        girdsOptions: {},
        data: [],
        column: [],
      },
    },
    /* 表单状态 */
    formStatus: 0, // 1 新增 2 修改
  };
  return initCRUD(Object.assign({}, data));
};
// }
/**
 * @vuese
 * @Description: 合并参数  说明：如果src有该参数则覆盖src的参数，没有则不操作。
 * @arg {Object} src  // 来源
 * @arg {Object} params //目标
 * @function: mergeParams
 */
function mergeParams(src, params) {
  const optsRet = {
    ...src,
  };
  for (const key in src) {
    if (params.hasOwnProperty(key)) {
      optsRet[key] = params[key];
    }
  }
  return optsRet;
}
export default CRUD;
