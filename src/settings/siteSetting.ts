// github repo url
export const GITHUB_URL = 'https://gitee.com/genCode/gen-code-admin';

// doc
export const DOC_URL = 'https://gitee.com/genCode/gen-code-admin';

// site url
export const SITE_URL = 'https://gitee.com/genCode/gen-code-admin';
