import {AppRouteModule} from "/@/router/types";
import {LAYOUT} from "/@/router/constant";

const tool: AppRouteModule = {
  path: '/tool',
  name: 'Tool',
  component: LAYOUT,
  redirect: '/tool/page',
  meta: {
    orderNo: 15,
    icon: 'ion:key-outline',
    title: '系统工具',
  },
  children: [
    {
      path: 'datasource',
      name: 'DataSource',
      meta: {
        title: '数据源管理',
        icon: 'ant-design:database-filled',
        ignoreKeepAlive: false,
      },
      component: () => import('/@/views/tool/datasource/index.vue'),
    },
    {
      path: 'gen',
      name: 'Gen',
      meta: {
        title: '代码生成器',
        icon: 'ant-design:codepen-outlined',
        ignoreKeepAlive: false,
      },
      component: () => import('/@/views/tool/gen/index.vue'),
    },
    {
      path: 'edit-table/:tableId',
      name: 'EditTable',
      meta: {
        title: '修改代码配置',
        carryParam: true,
        hideMenu: true,
        ignoreKeepAlive: false,
      },
      component: () => import('/@/views/tool/gen/edit/index.vue'),
    },
    {
      path: 'tpl',
      name: 'Tpl',
      meta: {
        title: '生成模板管理',
        icon: 'ant-design:folder-open-outlined',
        ignoreKeepAlive: false,
      },
      component: () => import('/@/views/tool/tpl/index.vue'),
    },
    // {
    //   path: 'templateEdit',
    //   name: 'templateEdit',
    //   meta: {
    //     title: '模板管理',
    //     icon: 'ant-design:folder-open-outlined',
    //     ignoreKeepAlive: false,
    //   },
    //   component: () => import('/@/views/tool/templateEdit/index.vue'),
    // },
  ],
};

export default tool;
