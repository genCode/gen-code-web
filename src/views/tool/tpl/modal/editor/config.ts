/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\views\tool\tpl\modal\editor\config.ts
 */
import { FormSchema } from '/@/components/Table';
export const getFormConfig = (isUpdate: boolean): FormSchema[] => [
  {
    field: 'name',
    label: '模板名称',
    component: 'Input',
    required: true,
    dynamicDisabled: () => (isUpdate && true) || false,
    colProps: {
      span: 16,
    },
  },
  {
    field: 'parentPath',
    label: '路径',
    component: 'Input',
    required: true,
    dynamicDisabled: () => true,
    colProps: {
      span: 16,
    },
  },
  {
    field: 'isDir',
    label: '是否是文件夹',
    component: 'Select',
    defaultValue: true,
    dynamicDisabled: () => (isUpdate && true) || false,
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    required: true,
    colProps: {
      span: 16,
    },
  },
  {
    label: '模板内容',
    field: 'content',
    colSlot: 'content',
    component: 'Input',
    ifShow: ({ values }) => {
      return values.isDir == false;
    },
    colProps: {
      span: 24,
    },
  },
];
