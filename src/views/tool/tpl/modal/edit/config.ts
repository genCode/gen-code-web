/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\views\tool\tpl\modal\edit\config.ts
 */
import { FormSchema } from '/@/components/Table';
export const getFormConfig = (): FormSchema[] => [
  {
    field: 'newName',
    label: '模板名称',
    component: 'Input',
    required: true,
    colProps: {
      span: 16,
    },
  },
];
