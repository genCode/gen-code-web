/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\views\tool\tpl\modal\settings\config.ts
 */
import { FormSchema } from '/@/components/Table';
export const getFormConfig = (): FormSchema[] => [
  {
    field: 'name',
    label: '模板名称',
    component: 'Input',
    dynamicDisabled: () => true,
    required: true,
    defaultValue: '',
    colProps: {
      span: 16,
    },
  },
  {
    label: '模板内容',
    field: 'content',
    colSlot: 'content',
    component: 'Input',
    colProps: {
      span: 24,
    },
  },
];
