export const columns = [
  { type: 'checkbox', width: 60 },
  {
    title: '名称',
    field: 'name',
    treeNode: true,
  },
  {
    title: '路径',
    field: 'path',
  },
  {
    title: '修改时间',
    field: 'modifyTime',
  },
];

// 右键
export const contextMenu = (xTable, isSuper: Boolean) => {
  return {
    className: 'contextMenu',
    body: {
      options: [
        [
          { code: 'add', name: '新增', disabled: false },
          { code: 'edit', name: '编辑', disabled: false },
          { code: 'rename', name: '重命名', disabled: false },
          { code: 'del', name: '删除', disabled: false },
          { code: 'settings', name: '模板设置', disabled: false },
        ],
        [
          { code: 'expand', name: '展开节点', disabled: false },
          { code: 'contract', name: '收起节点', disabled: false },
        ],
      ],
    },
    visibleMethod({ row, type, options }) {
      const $table = xTable.value;
      if (!isSuper) return;
      if (type === 'body') {
        options.forEach((list) => {
          list.forEach((item) => {
            switch (item.code) {
              default:
                item.disabled = !row && !['add'].includes(item.code) ? true : false;
                break;
              case 'edit':
                item.disabled = (row?.type !== 2 && true) || false;
                break;
              // 展开和收起
              case 'expand':
              case 'contract':
                if (row?.children?.length > 0) {
                  const isExpand = $table.isTreeExpandByRow(row);
                  item.disabled = item.code === 'expand' ? isExpand : !isExpand;
                } else {
                  item.disabled = true;
                }
                break;
              case 'settings':
              // 重命名
              case 'rename':
                item.disabled = (row?.type !== 0 && true) || false;
                break;
            }
            // if (!row && !['add'].includes(item.code)) item.disabled = true;
          });
        });
      }
      return true;
    },
  };
};
