/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\views\tool\tpl\api\model.ts
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
export interface itemInter {
  name: string;
  path: string;
  isDir: boolean;
  modifyTime: Date;
  children: itemInter[];
}
export interface delInter {
  newName: string;
  path: string;
}
export type resultType = BasicFetchResult<itemInter>;
