/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\views\tool\tpl\api\index.ts
 */
import { defHttp } from '/@/utils/http/axios';
import { useGlobSetting } from '/@/hooks/setting';
import { resultType, delInter } from './model';
const { genTableUrl = '' } = useGlobSetting();
// 获取树
export const getAPI = (params) => {
  return defHttp.get<resultType>(
    {
      url: `/tool/tpl/tree`,
      params,
    },
    {
      apiUrl: genTableUrl,
      isReturnNativeResponse: true,
    },
  );
};
// 新增模板
export const addAPI = (params) => {
  return defHttp.post<string>(
    {
      url: `/tool/tpl/add/tpl`,
      params,
    },
    {
      apiUrl: genTableUrl,
      successMessageMode:"message",
      isReturnNativeResponse: true,
    },
  );
};

// 删除模板
export const delAPI = (params: delInter) => {
  return defHttp.delete<string>(
    {
      url: `/tool/tpl/del/tpl`,
      params,
    },
    {
      apiUrl: genTableUrl,
      successMessageMode:"message",
      isReturnNativeResponse: true,
    },
  );
};
// 重命名模板
export const editAPI = (params: delInter) => {
  return defHttp.post<string>(
    {
      url: `/tool/tpl/rename`,
      params,
    },
    {
      apiUrl: genTableUrl,
      successMessageMode:"message",
      isReturnNativeResponse: true,
    },
  );
};
// 详情
export const detailAPI = (params) => {
  return defHttp.get<string>(
    {
      url: `/tool/tpl/getContent`,
      params,
    },
    {
      apiUrl: genTableUrl,
      isReturnNativeResponse: true,
    },
  );
};

// 新增文件
export const addFileAPI = (params) => {
  return defHttp.post<string>(
    {
      url: `/tool/tpl/add/file`,
      params,
    },
    {
      apiUrl: genTableUrl,
      successMessageMode:"message",
      isReturnNativeResponse: true,
    },
  );
};
// 新增文件
export const editFileAPI = (params) => {
  return defHttp.put<string>(
    {
      url: `/tool/tpl/saveContent`,
      params,
    },
    {
      apiUrl: genTableUrl,
      successMessageMode:"message",
      isReturnNativeResponse: true,
    },
  );
};
// 删除文件
export const delFileAPI = (params) => {
  return defHttp.delete<string>(
    {
      url: `/tool/tpl/del/file`,
      params,
    },
    {
      apiUrl: genTableUrl,
      successMessageMode:"message",
      isReturnNativeResponse: true,
    },
  );
};

// 获取设置
export const getSettingsAPI = (params) => {
  return defHttp.get<string>(
    {
      url: `/tool/tpl/settings`,
      params,
    },
    {
      apiUrl: genTableUrl,
      isReturnNativeResponse: true,
    },
  );
};
export const saveSettingsAPI = (params) => {
  return defHttp.put<string>(
    {
      url: `/tool/tpl/settings`,
      params,
    },
    {
      apiUrl: genTableUrl,
      successMessageMode:"message",
      isReturnNativeResponse: true,
    },
  );
};
