/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\views\tool\gen\edit\config.ts
 */
import { getOptions } from './enum';
import { FormSchema } from '/@/components/Form';
import { TableInfoItem } from '../api/model';
/*
 * vxeTable 自定义组件 - start
 */
const switchOpts = {
  cellRender: {
    name: 'ASwitch',
    props: {
      'checked-children': '开',
      'un-checked-children': '关',
    },
  },
  formatter: ({ cellValue }) => {
    return cellValue == 1 ? '是' : '否';
  },
};
const inputOpts = (field) => {
  return {
    editRender: {
      name: 'AInput',
      props: { allowClear: true, placeholder: `请输入${field}` },
    },
  };
};
/*
 * vxeTable 自定义组件 -end
 */
/*
 * vxeTable 列配置
 */
export function getColumn(event) {
  switch (event) {
    case 'edit':
      return [
        { field: 'tableId', title: '列ID', visible: false },
        { field: 'columnId', title: '表ID', visible: false },
        {
          field: 'columnName',
          title: '字段名',
          fixed: 'left',
          ...inputOpts('字段名'),
        },
        {
          field: 'columnComment',
          title: '字段描述',
          fixed: 'left',
          ...inputOpts('字段描述'),
        },
        { field: 'columnType', title: '字段类型' },
        { field: 'columnLength', title: '字段长度' },
        {
          field: 'javaType',
          title: 'java类型',
          editRender: {
            name: 'ASelect',
            options: getOptions('javaType'),
          },
        },
        {
          field: 'javaField',
          title: 'java属性',
          ...inputOpts('java属性'),
        },
        {
          field: 'goType',
          title: 'go类型',
          editRender: {
            name: 'ASelect',
            options: getOptions('goType'),
          },
        },
        {
          field: 'goField',
          title: 'go属性',
          ...inputOpts('go属性'),
        },
        {
          field: 'isInsert',
          title: '插入',
          ...switchOpts,
        },
        {
          field: 'isEdit',
          title: '编辑',
          ...switchOpts,
        },
        {
          field: 'isList',
          title: '列表',
          ...switchOpts,
        },
        {
          field: 'isQuery',
          title: '查询',
          ...switchOpts,
        },
        {
          field: 'queryType',
          title: '查询方式',
          editRender: {
            name: 'ASelect',
            options: getOptions('queryType'),
          },
        },
        {
          field: 'isRequired',
          title: '必填',
          ...switchOpts,
        },
        {
          field: 'htmlType',
          title: '显示类型',
          editRender: {
            name: 'ASelect',
            options: getOptions('showType'),
          },
        },
      ];
  }
}

export function getFormConfig(type: string, params?: { treeOptions: any; tableInfo: any }) {
  switch (type) {
    case 'gen':
      /*
       * 生成信息
       */
      return [
        {
          field: 'systemName',
          component: 'Input',
          label: '生成系统名',
          required: false,
        },
        {
          field: 'packageName',
          component: 'Input',
          label: '生成包路径',
          required: false,
        },
        {
          field: 'moduleName',
          component: 'Input',
          label: '生成模块名',
          required: false,
        },
        {
          field: 'businessName',
          component: 'Input',
          label: '生成业务名',
          required: true,
        },
        {
          field: 'functionName',
          component: 'Input',
          label: '生成功能名',
          required: true,
        },
        {
          field: 'tplCategory',
          component: 'Select',
          label: '生成模板',
          required: true,
          componentProps: {
            options: getOptions('tpl'),
          },
        },
        {
          labelWidth: 120,
          field: 'subTableName',
          component: 'Select',
          label: '关联子表的表名',
          required: true,
          componentProps: {
            options:
              (params?.tableInfo?.tables.map((value) => {
                return {
                  label: `${value.tableName}:${value.tableComment}`,
                  value: value.tableName,
                };
              }) as LabelValueOptions) || [],
          },
          ifShow: ({ values }) => {
            return values.tplCategory == 'sub';
          },
        },
        {
          labelWidth: 120,
          field: 'subTableFkName',
          component: 'Select',
          label: '子表关联的外键名',
          required: true,
          componentProps: (res) => {
            let value: TableInfoItem | undefined = params?.tableInfo.tables?.find((item) => item.tableName == res.formModel.subTableName);
            return {
              options:
                (value?.columns.map((value) => {
                  return {
                    label: `${value.columnName}:${value.columnComment}`,
                    value: value.columnName,
                  };
                }) as LabelValueOptions) || [],
            };
          },
          ifShow: ({ values }) => {
            return !!values.subTableName;
          },
        },
        {
          field: 'treeCode',
          component: 'Select',
          label: '树编码字段',
          required: true,
          componentProps: {
            options: params?.treeOptions,
          },
          ifShow: ({ values }) => {
            return values.tplCategory == 'tree';
          },
        },
        {
          field: 'treeParentCode',
          component: 'Select',
          label: '树父编码字段',
          required: true,
          componentProps: {
            options: params?.treeOptions,
          },
          ifShow: ({ values }) => {
            return values.tplCategory == 'tree';
          },
        },
        {
          field: 'treeName',
          component: 'Select',
          label: '树名称字段',
          required: true,
          componentProps: {
            options: params?.treeOptions,
          },
          ifShow: ({ values }) => {
            return values.tplCategory == 'tree';
          },
        },
      ] as FormSchema[];
    case 'base':
      /*
       * 基本信息
       */
      return [
        {
          field: 'tableName',
          component: 'Input',
          label: '表名称',
          required: true,
        },
        {
          field: 'tableComment',
          component: 'Input',
          label: '表描述',
          required: true,
        },
        {
          field: 'className',
          component: 'Input',
          label: '实体类名称',
          required: true,
        },
        {
          field: 'functionAuthor',
          component: 'Input',
          label: '作者',
          required: false,
        },
        {
          field: 'remark',
          component: 'InputTextArea',
          label: '备注',
          colProps: {
            span: 21,
          },
        },
      ] as FormSchema[];
  }
}
