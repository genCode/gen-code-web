export const getOptions = (type) => {
  switch (type) {
    case 'queryType':
      return [
        {
          label: '=',
          value: 'EQ',
        },
        {
          label: '!=',
          value: 'NE',
        },
        {
          label: '>',
          value: 'GT',
        },
        {
          label: '>=',
          value: 'GTE',
        },
        {
          label: '<',
          value: 'LT',
        },
        {
          label: '<=',
          value: 'LTE',
        },
        {
          label: 'LIKE',
          value: 'LIKE',
        },
        {
          label: 'BETWEEN',
          value: 'BETWEEN',
        },
      ] as LabelValueOptions;
    case 'showType':
      return [
        {
          label: '文本框',
          value: 'input',
        },
        {
          label: '文本域',
          value: 'textarea',
        },
        {
          label: '下拉框',
          value: 'select',
        },
        {
          label: '单选框',
          value: 'radio',
        },
        {
          label: '复选框',
          value: 'checkbox',
        },
        {
          label: '日期控件',
          value: 'datetime',
        },
        {
          label: '图片上传',
          value: 'imageUpload',
        },
        {
          label: '文件上传',
          value: 'fileUpload',
        },

        {
          label: '富文本控件',
          value: 'editor',
        },
      ] as LabelValueOptions;
    case 'javaType':
      return [
        {
          label: 'Long',
          value: 'Long',
        },
        {
          label: 'String',
          value: 'String',
        },
        {
          label: 'Integer',
          value: 'Integer',
        },
        {
          label: 'Double',
          value: 'Double',
        },
        {
          label: 'BigDecimal',
          value: 'BigDecimal',
        },
        {
          label: 'Date',
          value: 'Date',
        },
      ] as LabelValueOptions;
    case 'goType':
      return [
        {
          label: 'int',
          value: 'int',
        },
        {
          label: 'int8',
          value: 'int8',
        },
        {
          label: 'int16',
          value: 'int16',
        },
        {
          label: 'int32',
          value: 'int32',
        },
        {
          label: 'int64',
          value: 'int64',
        },
        {
          label: 'uint',
          value: 'uint',
        },
        {
          label: 'uint8',
          value: 'uint8',
        },
        {
          label: 'uint16',
          value: 'uint16',
        },
        {
          label: 'uint32',
          value: 'uint32',
        },
        {
          label: 'uint64',
          value: 'uint64',
        },
        {
          label: 'float32',
          value: 'float32',
        },
        {
          label: 'float64',
          value: 'float64',
        },
        {
          label: 'string',
          value: 'string',
        },
        {
          label: '*gtime.Time',
          value: '*gtime.Time',
        },
      ] as LabelValueOptions;
    case 'tpl':
      return [
        {
          label: '单表（增删改查）',
          value: 'crud',
        },
        {
          label: '树表（增删改查）',
          value: 'tree',
        },
        {
          label: '主子表（增删改查）',
          value: 'sub',
        },
      ] as LabelValueOptions;
  }
};
