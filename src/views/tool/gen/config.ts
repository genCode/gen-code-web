import { FormProps } from '/@/components/Table';
import { BasicColumn } from '/@/components/Table/src/types/table';
import { getAPI as getDatasource } from '/@/views/tool/datasource/api';

export function getBasicColumns(): BasicColumn[] {
  return [
    {
      title: '表ID',
      dataIndex: 'tableId',
      defaultHidden: true,
    },
    {
      title: '表名称',
      dataIndex: 'tableName',
      width: 150,
    },
    {
      title: '表描述',
      dataIndex: 'tableComment',
      width: 150,
    },
    {
      title: '实体类名称',
      dataIndex: 'ClassName',
      width: 150,
      defaultHidden: true,
    },
    {
      title: '创建时间',
      width: 150,
      dataIndex: 'createTime',
    },
    {
      title: '更新时间',
      width: 150,
      dataIndex: 'updateTime',
    },
  ];
}

export function getDBTables(): BasicColumn[] {
  return [
    {
      title: '表名称',
      dataIndex: 'tableName',
      width: 150,
    },
    {
      title: '表描述',
      dataIndex: 'tableComment',
      width: 150,
    },
    {
      title: '创建时间',
      width: 150,
      dataIndex: 'createTime',
    },
    {
      title: '更新时间',
      width: 150,
      dataIndex: 'updateTime',
    },
  ];
}

export function getDbFormConfig(): Partial<FormProps> {
  return {
    labelWidth: 100,
    schemas: [
      {
        field: 'group',
        label: '数据源',
        component: 'ApiSelect',
        componentProps: ({ tableAction, formModel }) => {
          return {
            api: getDatasource,
            params: {},
            resultField: 'items',
            labelField: 'group',
            valueField: 'group',
            onChange: (event) => {
              const { reload } = tableAction;
              formModel.group = event;
              reload();
            },
          };
        },
        colProps: {
          span: 8,
        },
      },
      {
        field: 'tableName',
        label: '表名称',
        component: 'Input',
        colProps: {
          span: 8,
        },
      },
      {
        field: 'tableComment',
        label: '表描述',
        component: 'Input',
        colProps: {
          span: 8,
        },
      },
    ],
  };
}

export function getFormConfig(): Partial<FormProps> {
  return {
    labelWidth: 100,
    fieldMapToTime: [['searchTime', ['beginTime', 'endTime'], 'YYYY-MM-DD']],
    schemas: [
      // {
      //   field: 'tpl',
      //   label: '模板',
      //   component: 'ApiSelect',
      //   componentProps: {
      //     api: getTplList,
      //     params: {},
      //     resultField: 'items',
      //     labelField: 'label',
      //     valueField: 'value',
      //   },
      //   colProps: {
      //     span: 5,
      //   },
      //   defaultValue: 'java'
      // },
      {
        field: 'tableName',
        label: '表名称',
        component: 'Input',
        colProps: {
          span: 5,
        },
      },
      {
        field: 'tableComment',
        label: '表描述',
        component: 'Input',
        colProps: {
          span: 5,
        },
      },
      {
        field: 'searchTime',
        label: '表时间',
        component: 'RangePicker',
        componentProps: () => {
          return {
            format: 'YYYY-MM-DD',
            placeholder: ['开始时间', '结束时间'],
          };
        },
        colProps: {
          span: 8,
        },
      },
    ],
  };
}
