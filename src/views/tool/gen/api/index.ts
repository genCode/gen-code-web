/*
 * @version:
 * @Company:
 * @Description: 代码生成器api
 * @FilePath: \src\views\tool\gen\api\index.ts
 */
import { defHttp } from '/@/utils/http/axios';
import { useGlobSetting } from '/@/hooks/setting';
import { getParams, resultType, paramsPageType, PreviewParams, PreviewResultModel, GenCodeParams, DBTableParams, ListResultModel, DetailsResultModel } from './model';
const { genTableUrl = '' } = useGlobSetting();
enum Api {
  Crud = '/tool/gen',
}
/**
 * @vuese
 * @Description: 获取列表
 * @arg {getParams} params
 */
export function getAPI(params: getParams) {
  return defHttp.post<ListResultModel>(
    {
      url: `${Api.Crud}/pageList`,
      params,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}
/**
 * @vuese
 * @Description: 编辑
 * @arg {any} params
 */
export function editAPI(params: any) {
  return defHttp.put<string>(
    {
      url: `${Api.Crud}`,
      params,
    },
    {
      apiUrl: genTableUrl,
      successMessageMode:"message",
    },
  );
}
/**
 * @vuese
 * @Description: 获取详情
 * @arg {string} id
 */
export function getDetailAPI(id: string) {
  return defHttp.get<DetailsResultModel>(
    {
      url: `${Api.Crud}/${id}`,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}
/**
 * @vuese
 * @Description: 删除
 * @arg {object} params
 */
export const delAPI = (params: IdsType) => {
  return defHttp.delete<string>(
    {
      url: `${Api.Crud}`,
      params,
    },
    {
      apiUrl: genTableUrl,
      isReturnNativeResponse: true,
      successMessageMode:"message",
    },
  );
};
/**
 * @vuese
 * @Description: 获取模板列表
 * @arg {paramsPageType} params
 */
export const getTplList = (params?: paramsPageType) => {
  return defHttp.post<resultType>(
    {
      url: `/tool/tpl/list`,
      params,
    },
    {
      apiUrl: genTableUrl,
    },
  );
};
/**
 * @vuese
 * @Description: 预览代码
 * @arg {PreviewParams} params
 */
export function previewAPI(params: PreviewParams) {
  return defHttp.post<PreviewResultModel>(
    {
      url: `${Api.Crud}/preview/`,
      params,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}

/**
 * @vuese
 * @Description: 生成代码
 * @arg {GenCodeParams} params
 */
export function batchGenCode(params: GenCodeParams) {
  return defHttp.post<any>(
    {
      url: `${Api.Crud}/batchGenCode`,
      responseType: 'blob',
      params,
    },
    {
      apiUrl: genTableUrl,
      isTransformResponse: false,
      successMessageMode:"message",
    },
  );
}
/**
 * @vuese
 * @Description: 创表
 * @arg {object} params
 */
export function createAPI(params: { sql: string }) {
  return defHttp.post<string>(
    {
      url: `${Api.Crud}/createTable`,
      params,
    },
    {
      apiUrl: genTableUrl,
      successMessageMode:"message",
    },
  );
}

/**
 * @vuese
 * @Description: 导入表
 * @arg {object} params
 */
export function importAPI(params: { group: string; tables: string[] }) {
  return defHttp.post<string>(
    {
      url: `${Api.Crud}/importTable`,
      params,
    },
    {
      apiUrl: genTableUrl,
      successMessageMode:"message",
    },
  );
}

/**
 * @vuese
 * @Description: 获取导入列表
 * @arg {DBTableParams} params
 */
export function getDBPageList(params: DBTableParams) {
  return defHttp.post<ListResultModel>(
    {
      url: `${Api.Crud}/db/list`,
      params,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}
