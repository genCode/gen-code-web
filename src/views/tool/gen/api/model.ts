/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\views\tool\gen\api\model.ts
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
export type paramsPageType = BasicPageParams & {
  name?: string;
};
export type getParams = BasicPageParams & {
  tableName?: string;
  tableComment?: string;
  startTime?: Date;
  endTime?: Date;
};
export interface TableItem {
  tableId: number | string;
  tableName: string;
  tableComment: string;
  subTableName: string;
  subTableFkName: string;
  className: string;
  tplCategory: string;
  packageName: string;
  moduleName: string;
  businessName: string;
  functionName: string;
  functionAuthor: string;
  genType: string;
  genPath: string;
  options: string;
  createBy: string;
  createTime: Date;
  updateBy: string;
  updateTime: Date;
  remark: string;
}
export interface ColumnItem {
  columnId: number;
  tableId: number;
  columnName: string;
  columnComment: string;
  columnType: string;
  columnLength: number;
  javaType: string;
  javaField: string;
  goType: string;
  goField: string;
  isPk: boolean;
  isIncrement: boolean;
  isRequired: boolean;
  isInsert: boolean;
  isEdit: boolean;
  isList: boolean;
  isQuery: boolean;
  queryType: string;
  htmlType: string;
  dictType: string;
  sort: number;
  createBy: string;
  createTime: Date;
  updateBy: string;
  updateTime: string;
}
export type TableInfoItem = TableItem & {
  columns: ColumnItem[];
};
export interface optionsItem {
  label: string;
  value: string;
}
export type resultType = BasicFetchResult<LabelValueOptions | optionsItem>;

export interface PreviewParams {
  tableId: string | number;
  tplList: LabelValueOptions;
}
export interface PreviewResultModel {
  data: Map<string, string>;
}
export type DBTableParams = BasicPageParams & {
  tableName?: string;
  tableComment?: string;
};
export type GenCodeParams = IdsType & {
  tplList: string;
};
export type ListResultModel = BasicFetchResult<TableItem>;

/**
 * @vuese
 * @Description: 详情
 * @arg {*}
 */
export type DetailsResultModel = {
  info?: TableInfoItem;
  tables?: TableInfoItem[];
  items?: ColumnItem[];
};
