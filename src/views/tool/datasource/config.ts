import { FormProps, FormSchema } from '/@/components/Table';
import { BasicColumn } from '/@/components/Table/src/types/table';

export function getBasicColumns(): BasicColumn[] {
  return [
    {
      title: 'ID',
      dataIndex: 'id',
      fixed: 'left',
      width: 100,
      defaultHidden: true,
    },
    {
      title: '分组',
      dataIndex: 'group',
      fixed: 'left',
      width: 150,
    },
    {
      title: '地址',
      dataIndex: 'host',
      width: 150,
    },
    {
      title: '端口',
      dataIndex: 'port',
      width: 150,
    },
    {
      title: '账号',
      width: 150,
      dataIndex: 'user',
    },
    {
      title: '数据库名称',
      width: 150,
      dataIndex: 'name',
    },
    {
      title: '数据库类型',
      width: 150,
      dataIndex: 'type',
    },
  ];
}

export function getFormConfig(): Partial<FormProps> {
  return {
    labelWidth: 100,
    schemas: [
      {
        field: 'group',
        label: '分组',
        component: 'Input',
        colProps: {
          xl: 12,
          xxl: 8,
        },
      },
      {
        field: 'name',
        label: '名称',
        component: 'Input',
        colProps: {
          xl: 12,
          xxl: 8,
        },
      },
    ],
  };
}

export function formSchema(isUpdate: boolean): FormSchema[] {
  return [
    {
      field: 'id',
      label: 'id',
      component: 'Input',
      ifShow: false,
    },
    {
      field: 'group',
      label: '分组',
      component: 'Input',
      required: true,
      colProps: {
        span: 16,
      },
    },
    {
      field: 'host',
      label: '地址',
      component: 'Input',
      required: true,
      colProps: {
        span: 16,
      },
    },
    {
      field: 'port',
      label: '端口',
      component: 'InputNumber',
      required: true,
      colProps: {
        span: 16,
      },
    },
    {
      field: 'user',
      label: '账号',
      component: 'Input',
      required: true,
      colProps: {
        span: 16,
      },
    },
    {
      field: 'pass',
      label: '密码',
      component: 'InputPassword',
      required: true,
      ifShow: !isUpdate,
      colProps: {
        span: 16,
      },
    },
    {
      field: 'name',
      label: '数据库名',
      component: 'Input',
      required: true,
      colProps: {
        span: 16,
      },
    },
    {
      field: 'type',
      label: '类型',
      component: 'Select',
      defaultValue: 'mysql',
      componentProps: {
        options: [
          { label: 'mysql', value: 'mysql' },
          { label: 'oracle', value: 'oracle' },
          { label: 'pgsql', value: 'pgsql' },
          { label: 'mssql', value: 'mssql' },
        ],
      },
      colProps: {
        span: 16,
      },
      required: true,
    },
    {
      label: '备注',
      field: 'remark',
      component: 'InputTextArea',
      colProps: {
        span: 16,
      },
    },
  ];
}
