/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\views\tool\datasource\api\model.ts
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

/**
 * @vuese
 * @Description: 分页
 * @arg {*}
 */
export type paramsPageType = BasicPageParams & {
  group?: string;
  name?: string;
};

/**
 * @vuese
 * @Description: 数据源
 * @arg {*}
 */
export type dataSourceParams = {
  group?: string;
  name?: string;
};

/**
 * @vuese
 * @Description: 返回结果项
 * @arg {*}
 */

export interface itemInter {
  id: string;
  group: string;
  host: string;
  port: string;
  user: number;
  name: string;
  remark: string;
  createBy: string;
  createTime: string;
  updateBy: string;
  updateTime: string;
}

/**
 * @vuese
 * @Description: 返回结果
 * @arg {*}
 */
export type resultType = BasicFetchResult<itemInter>;
