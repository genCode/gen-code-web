/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\views\tool\datasource\api\index.ts
 */
import { defHttp } from '/@/utils/http/axios';
import { useGlobSetting } from '/@/hooks/setting';
import { paramsPageType, resultType, dataSourceParams, itemInter } from './model';
const { genTableUrl = '' } = useGlobSetting();
export interface delParamsInter {
  ids: string[] | number[];
}

/**
 * @vuese
 * @Description: 分页接口
 * @arg {paramsPageType} params
 */
export function getAPI(params: paramsPageType) {
  return defHttp.post<resultType>(
    {
      url: '/tool/db/pageList',
      params,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}

/**
 * @vuese
 * @Description: 获取数据源接口
 * @arg {paramsType} params
 */
export function getDataSourceAPI(params: dataSourceParams) {
  return defHttp.post<resultType>(
    {
      url: '/tool/db/list',
      params,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}

export function addAPI(params: itemInter) {
  return defHttp.post<string>(
    {
      url: '/tool/db',
      params,
    },
    {
      apiUrl: genTableUrl,
      successMessageMode: 'message',
    },
  );
}
/**
 * @vuese
 * @Description: 编辑
 * @arg {any} params
 */
export function editAPI(params: any) {
  return defHttp.put<string>(
    {
      url: `/tool/db`,
      params,
    },
    {
      apiUrl: genTableUrl,
      successMessageMode: 'message',
    },
  );
}
export function delAPI(params: delParamsInter) {
  return defHttp.delete<string>(
    {
      url: '/tool/db',
      params,
    },
    {
      apiUrl: genTableUrl,
      isReturnNativeResponse: true,
      successMessageMode: 'message',
    },
  );
}
export function testAPI(params: itemInter) {
  return defHttp.post<string>(
    {
      url: '/tool/db/edit/test',
      params,
    },
    {
      apiUrl: genTableUrl,
      isReturnNativeResponse: true,
    },
  );
}
