import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type TableParams = BasicPageParams & {
  tableName?: string;
  tableComment?: string;
  startTime?: Date;
  endTime?: Date;
};

export type DBTableParams = BasicPageParams & {
  tableName?: string;
  tableComment?: string;
};

export interface TableItem {
  tableId: number | string;
  tableName: string;
  tableComment: string;
  subTableName: string;
  subTableFkName: string;
  className: string;
  tplCategory: string;
  packageName: string;
  moduleName: string;
  businessName: string;
  functionName: string;
  functionAuthor: string;
  genType: string;
  genPath: string;
  options: string;
  createBy: string;
  createTime: Date;
  updateBy: string;
  updateTime: Date;
  remark: string;
}

export type TableInfoItem = TableItem & {
  columns: ColumnItem[];
};

export interface ColumnItem {
  columnId: number;
  tableId: number;
  columnName: string;
  columnComment: string;
  columnType: string;
  columnLength: number;
  javaType: string;
  javaField: string;
  goType: string;
  goField: string;
  isPk: boolean;
  isIncrement: boolean;
  isRequired: boolean;
  isInsert: boolean;
  isEdit: boolean;
  isList: boolean;
  isQuery: boolean;
  queryType: string;
  htmlType: string;
  dictType: string;
  sort: number;
  createBy: string;
  createTime: Date;
  updateBy: string;
  updateTime: string;
}

export interface TableDetailsItem {
  info?: TableInfoItem;
  tables?: TableInfoItem[];
  items?: ColumnItem[];
}

interface tplListItems {
  label: string;
  value: string;
}
export interface PreviewParams {
  tableId: string | number;
  tplList: Array<tplListItems>;
}

export interface PreviewData {
  data: Map<string, string>;
}

export interface CreateParams {
  sql: string;
}

export interface ImportParams {
  group: string;
  tables: string[];
}

export interface IdParams {
  ids: string[] | number[];
}

export type GenCodeParams = IdParams & {
  tplList: string;
};

export type TableListResultModel = BasicFetchResult<TableItem>;

export type TableDetailsResultModel = TableDetailsItem;

export type PreviewResultModel = PreviewData;
