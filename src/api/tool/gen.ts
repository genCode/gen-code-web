import {
  TableItem,
  TableParams,
  IdParams,
  TableListResultModel,
  CreateParams,
  ImportParams,
  DBTableParams,
  TableDetailsResultModel,
  PreviewResultModel,
  PreviewParams,
  GenCodeParams,
} from './model/genModel';
import { defHttp } from '/@/utils/http/axios';
import { useGlobSetting } from '/@/hooks/setting';

enum Api {
  TablePageList = '/tool/gen/pageList',
  Crud = '/tool/gen',
  BatchGenCode = '/tool/gen/batchGenCode',
  Create = '/tool/gen/createTable',
  DBPageList = '/tool/gen/db/list',
  ImportSave = '/tool/gen/importTable',
  Preview = '/tool/gen/preview/',
}

const { genTableUrl = '' } = useGlobSetting();

export function getTablePageList(params: TableParams) {
  return defHttp.post<TableListResultModel>(
    {
      url: Api.TablePageList,
      params,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}

export function getDBPageList(params: DBTableParams) {
  return defHttp.post<TableListResultModel>(
    {
      url: Api.DBPageList,
      params,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}

export function create(params: CreateParams) {
  return defHttp.post<string>(
    {
      url: Api.Create,
      params,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}

export function importSave(params: ImportParams) {
  return defHttp.post<string>(
    {
      url: Api.ImportSave,
      params,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}

export function get(id: string) {
  return defHttp.get<TableDetailsResultModel>(
    {
      url: `${Api.Crud}/${id}`,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}

export function add(params: TableItem) {
  return defHttp.post<string>(
    {
      url: Api.Crud,
      params,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}

export function preview(params: PreviewParams) {
  return defHttp.post<PreviewResultModel>(
    {
      url: `${Api.Preview}`,
      params,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}

export function edit(params: any) {
  return defHttp.put<string>(
    {
      url: Api.Crud,
      params,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}

export function del(params: IdParams) {
  return defHttp.delete<string>(
    {
      url: Api.Crud,
      params,
    },
    {
      apiUrl: genTableUrl,
    },
  );
}

export function batchGenCode(params: GenCodeParams) {
  return defHttp.post<any>(
    {
      url: Api.BatchGenCode,
      responseType: 'blob',
      params,
    },
    {
      apiUrl: genTableUrl,
      isTransformResponse: false,
    },
  );
}
