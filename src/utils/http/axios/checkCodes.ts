/*
 * @version:
 * @Company:
 * @Description:
 * @FilePath: \src\utils\http\axios\checkCodes.ts
 */
import type { ErrorMessageMode } from '/#/axios';
import { useMessage } from '/@/hooks/web/useMessage';
import { useI18n } from '/@/hooks/web/useI18n';

const { createMessage, createErrorModal, createSuccessModal } = useMessage();
const error = createMessage.error!;
const success = createMessage.success!;

/**
 * @vuese
 * @Description: 对code的拦截
 * @arg {number} code
 * @arg {string} msg
 * @arg {ErrorMessageMode} messageMode
 */
export function checkCodes(code: number, msg: string, messageMode: ErrorMessageMode = 'message'): void {
  const { t } = useI18n();
  let errMessage = ''; // 失败消息
  let successMessage = ''; // 成功消息

  switch (code) {
    case 200:
      successMessage = `${msg || '操作成功'}`;
      break;
    case 500:
      errMessage = `${msg || '操作失败'}`;
      break;
  }

  if (errMessage) {
    if (messageMode === 'modal') {
      createErrorModal({ title: t('sys.api.errorTip'), content: errMessage });
    } else if (messageMode === 'message') {
      error({ content: errMessage, key: `global_error_message_codes_${code}` });
    }
  }
  if (successMessage) {
    if (messageMode === 'modal') {
      createSuccessModal({ title: t('sys.api.successTip'), content: successMessage });
    } else if (messageMode === 'message') {
      success({ content: successMessage, key: `global_success_message_codes_${code}` });
    }
  }
}
